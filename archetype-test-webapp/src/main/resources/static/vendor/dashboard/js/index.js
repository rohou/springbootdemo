/**
 * Created by Administrator on 2016/12/28 0028.
 */
layui.use(['element','jquery','layer'], function(){
    var element = layui.element();
    var layer=layui.layer;
    var $=layui.jquery;
    var collpaseStatus=0;
    //一些事件监听

    element.on('nav(test)', function(data){
        var $a=$(this).find("a");
        var title=$a.html()
        var html ="<iframe class='my-frame' src='"+$a.data("href")+"'></iframe>"
        addMyTab(title,html)
    });
    
    $("#menu-collpase").click(function () {
        collpaseStatus++;
        collpaseStatus%=2;
        if (collpaseStatus===1){
            $("#menu-collpase").html("&#xe602;");
            $(".my-menue-collpase").css("left","0px");
            $(".my-header").css("margin-left","-200px");
        }else{
            $("#menu-collpase").html("&#xe603;");
            $(".my-menue-collpase").css("left","200px");
            $(".my-header").css("margin-left","0px");
        }
    })
    $("#user-more").click(function () {
        layer.open({
            type: 1
//                ,content: '<div style="padding: 20px 80px;">内容</div>'
            ,skin: 'layui-layer-molv'
            ,btnAlign: 'c' //按钮居中
            ,shade: 0.2 //不显示遮罩，
            ,anim:0
            ,btn: ['退出', '修改密码','取消']
            ,btn1:function (index, layero) {
                window.location.href=rootPath+"/logout";
                layer.closeAll();
            },btn2:function () {
                let html="<div style='padding: 15px'><input class='layui-input' type='password' id='repeat' lay-verify='required' placeholder='请输入旧密码'>" +
                    "<input style='margin-top: 10px' class='layui-input' type='password' id='new' lay-verify='required' placeholder='请输入新密码'>" +
                    "<button class='layui-btn '  style='width:100%;margin-top: 10px' id='updateDo'>立即修改</button></div>";
                layer.open({
                    type: 1 ,//Page层类型
                    title:'账号密码修改'
                    ,area: ['300px', '225px']
                    ,shade: 0.6 //遮罩透明度
                    ,maxmin: true //允许全屏最小化
                    ,anim: 1 //0-6的动画形式，-1不开启
                    ,content: html
                });
                $("#updateDo").click(function () {
                    $.ajax({
                        url:rootPath+"/user/password/update",
                        type:'get',
                        data:{
                            oldPass:$("#repeat").val(),
                            newPass:$("#new").val()
                        },
                        success:function (data) {
                            if(data.code==='200'){
                                layer.alert("修改成功");
                            }else{
                                layer.alert(data.msg);
                            }
                        },
                        error:function (XMLHttpRequest) {
                            if(XMLHttpRequest.status===415){
                                var data = JSON.parse(XMLHttpRequest.responseText);
                                var html="";
                                for (var i=0;i<data.length;i++){
                                    if (i!=0)
                                        html+=",";
                                    html+=data[i].message;
                                }
                                layer.alert(html)
                            }
                        }
                    });

                })
            }
        });
    })
    function addMyTab(title,content) {
        var $parent=$("div[lay-filter='tab']>ul");
        var $tab=$parent.find("li");
        for (var i=0;i<$tab.length;i++){
            var tmp=$tab[i].childNodes[0].outerHTML+$tab[i].childNodes[1].data;
            if (tmp===title){
                element.tabChange('tab', i);
                return ;
            }
        }
        element.tabAdd('tab', {
            title: title
            ,content: content //支持传入html
        });
        element.tabChange('tab', $tab.length);
    }
});

