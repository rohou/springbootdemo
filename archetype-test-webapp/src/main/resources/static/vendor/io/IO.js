/**
 * Created by Administrator on 2017/3/13 0013.
 */

$.extend({
    IOQuery: function(params) {
        var _default={
            error:function (XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status===415){
                    var data = JSON.parse(XMLHttpRequest.responseText);
                    var html="";
                    for (var i=0;i<data.length;i++){
                        if (i!=0)
                            html+=",";
                        html+=data[i].field+data[i].message;
                    }
                    swal("参数错误",html,"error");
                }
            }
        }
        var request=$.extend({},_default,params)
        return $.ajax(request);
    }
})