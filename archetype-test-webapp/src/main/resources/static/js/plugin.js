/**
 * Created by Administrator on 2017/3/1 0001.
 */
(function ($) {
    $.fn.extend({
        "paging":function (params,func) {
            let size=10;
            let tolPage=params.totalPages;
            let pageSize=params.pageSize;
            let page=params.pageNumber;
            let html ="<div id='_paging' class='pull-right pagination'>";
            html+="<ul class='pagination'>";
            html+="<li class='page-pre'><a href='javascript:void(0)'>‹</a></li>";
            for (let i=Math.max(1,page-size/2),k=0;k<size&&i<tolPage;k++,i++){
                if (i===page){
                    html+="<li class='page-number active'><a href='javascript:void(0)'>"+i+"</a></li>"
                }else{
                    html+="<li class='page-number'><a href='javascript:void(0)'>"+i+"</a></li>";
                }
            }
            html+="<li class='page-next'><a href='javascript:void(0)'>›</a></li></ul></div>";
            $(this).find("#_paging").remove();
            $(this).append(html);
            $("#_paging>ul>li").click(function () {
                if ($(this).hasClass("page-pre")){
                    page=Math.max(--page,1);
                }else if ($(this).hasClass("page-next")){
                    page=Math.min(++page,tolPage);
                }else{
                    page=parseInt($(this).find("a").html());
                }
                func(page);
            })
        },

        "timeDegree": function (fuc,type) {
            var btns=["<button name='_degree' data-val='hourly'style='padding: 2px 12px;' class='btn btn-default'>时</button>",
                "<button name='_degree' data-val='daily' style='padding: 2px 12px;' class='btn btn-default'>日</button>",
                "<button name='_degree' data-val='weekly' style='padding: 2px 12px;' class='btn btn-default'>周</button>",
                "<button name='_degree' data-val='monthly' style='padding: 2px 12px;' class='btn btn-default'>月</button>"]
            var html = " <div class='tn-group' id='_timeDegree'>";
            if (!type)
                html=html+btns[1]+btns[2]+btns[3]+"</div>";
            if(type==='stzy')
                html=html+btns[0]+btns[1]+btns[2]+btns[3]+"</div>";
            if(type==='dy')
                html=html+btns[1]+btns[3]+"</div>";
            $(this).append(html)
            $("#_timeDegree").find("button:first").removeClass("btn-default").addClass("btn-success");
            $("button[name='_degree']").click(function () {
                $("button.btn-success[name='_degree']").removeClass('btn-success').addClass('btn-default');
                $(this).removeClass("btn-default").addClass('btn-success');
                var value= $(this).data('val');
                fuc(value);
            })
        }
    });
    $.extend({
        formatTimeStamp:function (date){
            date = new Date (date);
            let yy = date.getFullYear();
            let MM = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);//获取当前月份的日期，不足10补0
            let dd = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            let HH = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
            let mm = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
            let ss = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
            return +yy+"-"+MM+"-"+dd+" "+HH+":"+mm+":"+ss;
        },
        formatTimeStampToDate:function (date){
            date = new Date (date);
            let yy = date.getFullYear();
            let MM = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);//获取当前月份的日期，不足10补0
            let dd = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            return yy+"-"+MM+"-"+dd;
        }
    });
})(jQuery);