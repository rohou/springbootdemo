package com.springboot.core.security;

import com.springboot.sql.dao.TSysMenuDao;
import com.springboot.sql.dao.entity.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * On 2017/2/27 0027.
 * 获取URL对应需要怎样的权限
 */
@Service
public class MyInvocationSecurityMetadataSourceService implements FilterInvocationSecurityMetadataSource {
    private final TSysMenuDao tSysMenuDao;
    private final FilterStatic filterStatic;
    private final HashSet<Pattern> patterns;
    @Autowired
    public MyInvocationSecurityMetadataSourceService(TSysMenuDao tSysMenuDao,FilterStatic filterStatic) {
        this.tSysMenuDao = tSysMenuDao;
        this.filterStatic = filterStatic;
        patterns = new HashSet<>();
        for (String filter:filterStatic.getStaticFilters()){
           String regex= filter.replace("**","*").replace("*",".*");
           patterns.add(Pattern.compile(regex));
        }
    }



    /**
     * 加载权限表中所有权限
     */
    public  Collection<ConfigAttribute> loadResourceDefine(String url){
        Collection<ConfigAttribute> array=new ArrayList<>();
        ConfigAttribute cfg;
        SysMenu permission = tSysMenuDao.findMeneRoles(url);
        if (permission !=null) {
            for (String role :permission.getRoles().split(",")){
                cfg = new SecurityConfig(role);
                //此处只添加了用户的名字，其实还可以添加更多权限的信息，例如请求方法到ConfigAttribute的集合中去。此处添加的信息将会作为MyAccessDecisionManager类的decide的第三个参数。
                array.add(cfg);
            }
            return array;
        }
        return null;

    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        //object 中包含用户请求的request 信息
        HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
        String url = request.getRequestURI();
        Iterator<Pattern> patternIterator=patterns.iterator();
        while (patternIterator.hasNext()){
            Pattern pattern = patternIterator.next();
            Matcher matcher=pattern.matcher(url);
            if (matcher.find())
                return null;
        }
        return loadResourceDefine(url);
    }


    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
