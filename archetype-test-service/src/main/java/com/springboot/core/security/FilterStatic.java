package com.springboot.core.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * On 2017/3/8 0008.
 */
@Component
@ConfigurationProperties(prefix = "system")
public class FilterStatic {
    private ArrayList<String> staticFilters;

    public ArrayList<String> getStaticFilters() {
        return staticFilters;
    }

    public void setStaticFilters(ArrayList<String> staticFilters) {
        this.staticFilters = staticFilters;
    }

    @Override
    public String toString() {
        return "FilterStatic{" +
                "staticFilters=" + staticFilters +
                '}';
    }
}
