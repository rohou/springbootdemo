package com.springboot.core.dictionary;

/**
 * On 2017/3/22 0022.
 */
public class DictItem {
    private String itemCode;
    private String itemName;
    private Integer itemOrder;

    public DictItem(String itemCode, String itemName, int itemOrder) {
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.itemOrder = itemOrder;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(Integer itemOrder) {
        this.itemOrder = itemOrder;
    }

    @Override
    public String toString() {
        return "DictItem{" +
                "itemCode='" + itemCode + '\'' +
                ", itemName='" + itemName + '\'' +
                ", itemOrder='" + itemOrder + '\'' +
                '}';
    }
}
