package com.springboot.sql.dao.entity;

/**
 * On 2017/2/24 0024.
 */
public class SysRole {
    /**
     * 角色是否拥有
     */
    private Boolean isHave;

    public Boolean getHave() {
        return isHave;
    }

    public void setHave(Boolean have) {
        isHave = have;
    }

    /**
     * 角色ID
     */
    private String roleId;
    /**
     * 角色名
     */
    private String roleName;
    /**
     * 角色标志
     */
    private String roleFlag;
    /**
     * 组织代码
     */
    private String orgCode;

    /**
     * 角色中文名
     */
    private String roleRealName;

    public String getRoleRealName() {
        return roleRealName;
    }

    public void setRoleRealName(String roleRealName) {
        this.roleRealName = roleRealName;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleFlag() {
        return roleFlag;
    }

    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "roleId='" + roleId + '\'' +
                ", roleName='" + roleName + '\'' +
                ", roleFlag='" + roleFlag + '\'' +
                ", orgCode='" + orgCode + '\'' +
                '}';
    }
}
