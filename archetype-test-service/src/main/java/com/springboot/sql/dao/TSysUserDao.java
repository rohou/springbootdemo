package com.springboot.sql.dao;

import com.springboot.core.base.BaseDao;
import com.springboot.sql.dao.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * On 2017/2/24 0024.
 */
public interface TSysUserDao extends BaseDao<SysUser> {
    SysUser findByUserName(String userName);

    /**
     * @param condition 多条件查找参数
     * @return 符合的用户列表
     */
    List<SysUser> find(SysUser condition);

    /**
     * @param username 用户帐号
     * @param roleIds 授权角色ID
     * @return 操作结果
     */
    int authRoles(@Param("username") String username,@Param("roleIds") List<String> roleIds);

    /**
     * @param userName 用户帐号
     * @param roleIds 需要取消授权的角色ID
     * @return 操作结果
     */
    int unAuthRoles(@Param("userName") String userName,@Param("roleIds") List<String> roleIds);

    /**
     * @param userName 需要删除的帐号
     * @return 操作结果
     */
    int deleteByUsername(@Param("userName") String userName);
}
