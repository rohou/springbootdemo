package com.springboot.sql.dao;

import com.springboot.sql.dao.entity.SysDictItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * On 2017/3/14 0014.
 */
public interface TsysDictItemDao {

    List<SysDictItem> findByEntryCode(@Param("entry") String entry);
}
