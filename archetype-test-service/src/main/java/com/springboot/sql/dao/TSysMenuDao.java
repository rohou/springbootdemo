package com.springboot.sql.dao;

import com.springboot.sql.dao.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * On 2017/2/24 0024.
 */
public interface TSysMenuDao {
    List<SysMenu> findAll();

    List<SysMenu> findMenuByRoles(@Param("roles") List<String> roles);

    SysMenu findMeneRoles(String httpAddress);

    List<SysMenu> findExt(SysMenu condition);

    /**
     * @param roleId 角色ID
     * @return 找出一系列角色拥有的菜单，包涵有和没有，用isHave做判断
     */
    List<SysMenu> findByRoleIsHaveMenuList(@Param("roleId") String roleId);
}
