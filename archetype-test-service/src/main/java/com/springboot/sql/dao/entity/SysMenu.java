package com.springboot.sql.dao.entity;

import java.util.List;

/**
 * On 2017/2/24 0024.
 */
public class SysMenu {

    /**
     * 标志对应role是否拥有此菜单
     */
    private Boolean isHave;

    public Boolean getHave() {
        return isHave;
    }

    public void setHave(Boolean have) {
        isHave = have;
    }

    /**
     * 子节点
     */
    private List<SysMenu> cSysMenus;

    private String roles;

    public List<SysMenu> getcSysMenus() {
        return cSysMenus;
    }

    public void setcSysMenus(List<SysMenu> cSysMenus) {
        this.cSysMenus = cSysMenus;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
    /**
     * 菜单名字
     */
    private String menuName;
    /**
     * 菜单ID
     */
    private String menuId;
    /**
     * 菜单类型
     */
    private String menuType;
    /**
     * 父菜单ID
     */
    private String menuPid;
    /**
     * 请求地址
     */
    private String httpAddress;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getMenuPid() {
        return menuPid;
    }

    public void setMenuPid(String menuPid) {
        this.menuPid = menuPid;
    }

    public String getHttpAddress() {
        return httpAddress;
    }

    public void setHttpAddress(String httpAddress) {
        this.httpAddress = httpAddress;
    }

    @Override
    public String toString() {
        return "SysMenu{" +
                "menuName='" + menuName + '\'' +
                ", menuId='" + menuId + '\'' +
                ", menuType='" + menuType + '\'' +
                ", menuPid='" + menuPid + '\'' +
                ", httpAddress='" + httpAddress + '\'' +
                '}';
    }
}
