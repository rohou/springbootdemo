package com.springboot.sql.dao.entity;

/**
 * On 2017/3/14 0014.
 */
public class SysDictItem {
    private String dictEntryCode;
    private String dictItemCode;
    private String dictItemName;
    private Integer dictItemOrder;

    public String getDictEntryCode() {
        return dictEntryCode;
    }

    public void setDictEntryCode(String dictEntryCode) {
        this.dictEntryCode = dictEntryCode;
    }

    public String getDictItemCode() {
        return dictItemCode;
    }

    public void setDictItemCode(String dictItemCode) {
        this.dictItemCode = dictItemCode;
    }

    public String getDictItemName() {
        return dictItemName;
    }

    public void setDictItemName(String dictItemName) {
        this.dictItemName = dictItemName;
    }

    public Integer getDictItemOrder() {
        return dictItemOrder;
    }

    public void setDictItemOrder(Integer dictItemOrder) {
        this.dictItemOrder = dictItemOrder;
    }

    @Override
    public String toString() {
        return "SysDictItem{" +
                "dictEntryCode='" + dictEntryCode + '\'' +
                ", dictItemCode='" + dictItemCode + '\'' +
                ", dictItemName='" + dictItemName + '\'' +
                ", dictItemOrder=" + dictItemOrder +
                '}';
    }
}
