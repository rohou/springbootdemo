package com.springboot.sql.dao;

import com.springboot.core.base.BaseDao;
import com.springboot.sql.dao.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * On 2017/2/28 0028.
 */
public interface TSysRoleDao extends BaseDao<SysRole>{
    /**
     * @param condition 条件集合 用AND相连
     * @return 符合的用户对象
     */
    List<SysRole> findCondition (SysRole condition);

    /**
     * @param id 用户id
     * @param name 用户名称  OR相连查询
     * @return 符合的用户对象
     */
    List<SysRole> findByIdOrName (@Param("id") String id, @Param("name") String name);

    /**
     * @param id 授权用户ID
     * @param menus 需要的权限列表
     * @return 成功与否
     */
    int authorizeRole(@Param("id") String id ,@Param("menus") List<String> menus);

    /**
     * @param roleId 授权用户ID
     * @param menus 需要删除的权限列表
     * @return  成功与否
     */
    int unAuthorizeRole (@Param("roleId") String roleId,@Param("menus") List<String> menus);

    /**
     * @param roleId 角色ID
     * @return 根据ID删除制定角色
     */
    int deleteById(@Param("roleId") String roleId);

    /**
     * @param username 用户帐号-唯一标识
     * @return 返回用户已拥有的和未拥有的角色列表
     */
    List<SysRole> findUserRoles (@Param("username") String username);
}
