package com.springboot.sql.service;

import com.springboot.sql.dao.entity.SysMenu;
import com.springboot.vo.res.Menu;
import com.springboot.vo.res.ZTree;

import java.util.List;

/**
 * On 2017/2/24 0024.
 */
public interface IMenuService {

    List<Menu> getMenuByRoles(List<String> roles);

    List<ZTree> getAllPermission4ZTree(String roleId);

}
