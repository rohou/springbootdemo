package com.springboot.sql.service;

import com.springboot.sql.dao.entity.SysDictItem;

import java.util.List;

/**
 * On 2017/3/22 0022.
 */
public interface IDictService {
    List<SysDictItem> find(String entry);
}
