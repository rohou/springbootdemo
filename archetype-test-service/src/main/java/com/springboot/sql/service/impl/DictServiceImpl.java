package com.springboot.sql.service.impl;

import com.springboot.sql.dao.TsysDictItemDao;
import com.springboot.sql.dao.entity.SysDictItem;
import com.springboot.sql.service.IDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * On 2017/3/22 0022.
 */
@Service
public class DictServiceImpl implements IDictService{
    private final TsysDictItemDao tsysDictItemDao;
    @Autowired
    public DictServiceImpl(TsysDictItemDao tsysDictItemDao) {
        this.tsysDictItemDao = tsysDictItemDao;
    }

    @Override
    public List<SysDictItem> find(String entry) {
        return tsysDictItemDao.findByEntryCode(entry);
    }
}
