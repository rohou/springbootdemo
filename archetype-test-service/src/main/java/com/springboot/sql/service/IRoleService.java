package com.springboot.sql.service;

import com.springboot.core.OperatorResult;
import com.springboot.sql.dao.entity.SysRole;
import com.springboot.vo.req.AddRoleReq;
import com.springboot.vo.req.UpdateRoleReq;

import javax.validation.Valid;
import java.util.List;

/**
 * On 2017/2/28 0028.
 */
public interface IRoleService {
    /**
     * @return 获取所有自定义角色
     */
    List<SysRole> getAllCustomRole();

    /**
     * @return 获取所有系统默认角色
     */
    List<SysRole> getAllSystemRole();

    /**
     * @param addRoleReq 新增角色的参数
     * @return  操作结果对象
     */
    OperatorResult addNewRole(@Valid AddRoleReq addRoleReq);

    /**
     * @param updateRoleReq 更新的角色参数
     * @return 操作结果对象
     */
    OperatorResult updateRole(@Valid UpdateRoleReq updateRoleReq);

    /**
     * @param roleIds 批量删除角色
     * @return 操作结果对象
     */
    OperatorResult deleteRole (List<String> roleIds);


}
