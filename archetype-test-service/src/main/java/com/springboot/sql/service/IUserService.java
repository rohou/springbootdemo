package com.springboot.sql.service;

import com.springboot.core.OperatorResult;
import com.springboot.vo.req.AddUserReq;
import com.springboot.vo.req.UpdateUserPasReq;
import com.springboot.vo.req.UpdateUserReq;
import com.springboot.vo.res.CusUserResp;
import com.springboot.vo.res.ZTree;

import javax.validation.Valid;
import java.util.List;

/**
 * On 2017/3/13 0013.
 */
public interface IUserService {
    /**
     * @return 所有自定义用户
     */
    List<CusUserResp> getAllCusUsers();

    /**
     * @param username 帐号-用户唯一标识
     * @return 用户所属的角色
     */
    List<ZTree> getUserRoles (String username);

    /**
     * @param addUserReq 添加用户的参数
     * @return 操作结果
     */
    OperatorResult insertUser(@Valid AddUserReq addUserReq);

    /**
     * @param updateUserReq 需要更新的用户参数
     * @return 操作结果
     */
    OperatorResult updateUser(@Valid UpdateUserReq updateUserReq);

    /**
     * @param usernames 需要删除的用户列表
     * @return  操作结果
     */
    OperatorResult deleteUser (List<String> usernames);
    /**
     * @param username 用户帐号
     * @return 操作结果
     */
    OperatorResult initPassWord(String username);

    /**
     * @param updateUserPasReq 需要更新的帐号及其密码
     * @return
     */
    OperatorResult updatePassWord(@Valid UpdateUserPasReq updateUserPasReq);
}
