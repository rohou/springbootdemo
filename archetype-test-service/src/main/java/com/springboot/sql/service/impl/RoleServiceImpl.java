package com.springboot.sql.service.impl;

import com.springboot.constants.RoleConstants;
import com.springboot.core.OperatorResult;
import com.springboot.sql.dao.TSysRoleDao;
import com.springboot.sql.dao.entity.SysRole;
import com.springboot.sql.service.IRoleService;
import com.springboot.utils.UuidUtils;
import com.springboot.vo.req.AddRoleReq;
import com.springboot.vo.req.UpdateItem;
import com.springboot.vo.req.UpdateRoleReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * On 2017/2/28 0028.
 */
@Service
public class RoleServiceImpl implements IRoleService {
    private final TSysRoleDao tSysRoleDao;
    @Autowired
    public RoleServiceImpl(TSysRoleDao tSysRoleDao) {
        this.tSysRoleDao = tSysRoleDao;
    }

    /**
     * @return 自定义角色集合
     */
    @Override
    public List<SysRole> getAllCustomRole() {
        SysRole condition= new SysRole();
        condition.setRoleFlag(RoleConstants.UNSYSTEM_FLAG);
        return tSysRoleDao.findCondition(condition);
    }

    /**
     * @return 系统默认角色集合
     */
    @Override
    public List<SysRole> getAllSystemRole() {
        SysRole condition = new SysRole();
        condition.setRoleFlag(RoleConstants.SYSTEM_FLAG);
        return tSysRoleDao.findCondition(condition);
    }

    @Override
    public OperatorResult addNewRole( AddRoleReq addRoleReq) {
        //判断是否已存在
        if (tSysRoleDao.findByIdOrName(null,addRoleReq.getRoleCode()).size()<1){
            SysRole sysRole = new SysRole();
            sysRole.setRoleId(UuidUtils.build());
            sysRole.setRoleName(addRoleReq.getRoleCode());
            sysRole.setRoleRealName(addRoleReq.getRoleRealName());
            sysRole.setRoleFlag(RoleConstants.UNSYSTEM_FLAG);
            sysRole.setOrgCode(addRoleReq.getOrgCode());
            if (tSysRoleDao.insert(sysRole)>0){
                if (addRoleReq.getNeedAddAuthorize().size()>0)
                    return tSysRoleDao.authorizeRole(sysRole.getRoleId(),addRoleReq.getNeedAddAuthorize())>0?OperatorResult.SUCCESS():OperatorResult.ERROR(RoleConstants.OPT_ROLE_AUTH_ERROR);
                else
                    return OperatorResult.SUCCESS();
            }
            return OperatorResult.ERROR(RoleConstants.OPT_ROLE_INSERT_ERROR);
        }
        return OperatorResult.ERROR(RoleConstants.OPT_ROLE_EXIST);
    }

    @Override
    public OperatorResult updateRole( UpdateRoleReq updateRoleReq) {
        //检查新角色名是否已经被占用
        List<SysRole> newName=tSysRoleDao.findByIdOrName(null,updateRoleReq.getRoleCode());
        //或者名字没有修改
        if (newName.size()<1||newName.get(0).getRoleId().equals(updateRoleReq.getRoleId())){
            SysRole condition =new  SysRole();
            condition.setRoleId(updateRoleReq.getRoleId());
            //只找自定义角色做修改
            condition.setRoleFlag(RoleConstants.UNSYSTEM_FLAG);
            List<SysRole> sysRoles=tSysRoleDao.findCondition(condition);
            if (sysRoles!=null&&sysRoles.size()>0){
                SysRole sysRole = sysRoles.get(0);
                //roleName 或者 roleCode 代表角色代码，用于代码层面区分不同角色
                sysRole.setRoleName(updateRoleReq.getRoleCode());
                // realName 为中文标识 方便用户观察
                sysRole.setRoleRealName(updateRoleReq.getRoleRealName());
                sysRole.setOrgCode(updateRoleReq.getOrgCode());
                if (tSysRoleDao.update(sysRole)>0){
                    //需要删除的权限
                    List<String> delAuth = new ArrayList<>();
                    //需要增加的权限
                    List<String> addAuth = new ArrayList<>();
                    for (UpdateItem updateItem:updateRoleReq.getUpdateAddAuthorize()){
                        if (updateItem.getChecked())
                            addAuth.add(updateItem.getId());
                        else
                            delAuth.add(updateItem.getId());
                    }
                    //如果删除或者增加列表为空，则不做操作
                    if (addAuth.size()>0)
                        tSysRoleDao.authorizeRole(sysRole.getRoleId(),addAuth);
                    if (delAuth.size()>0)
                        tSysRoleDao.unAuthorizeRole(sysRole.getRoleId(),delAuth);
                    return OperatorResult.SUCCESS();
                }
                return OperatorResult.ERROR(RoleConstants.OPT_ROLE_UPDATE_ERROR);
            }
            return OperatorResult.ERROR(RoleConstants.OPT_ROLE_NOEXIST);
        }
        return OperatorResult.ERROR(RoleConstants.OPT_ROLE_EXIST);
    }

    @Override
    public OperatorResult deleteRole(List<String> roleIds) {
        SysRole condition = new SysRole();
        condition.setRoleFlag(RoleConstants.UNSYSTEM_FLAG);
        for (String roleId : roleIds){
            condition.setRoleId(roleId);
            //验证是自定义角色
            if (tSysRoleDao.findCondition(condition).size()>0){
                tSysRoleDao.unAuthorizeRole(roleId,null);
                tSysRoleDao.deleteById(roleId);
            }
        }
        return OperatorResult.SUCCESS();
    }
}
