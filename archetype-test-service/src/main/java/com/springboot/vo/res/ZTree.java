package com.springboot.vo.res;

/**
 * On 2017/2/28 0028.
 * ZTree 对象集合
 */
public class ZTree {
    private String id;
    private String pId;
    private String name;
    private Boolean checked=false;

    public ZTree() {
    }

    public ZTree(String id, String pId, String name, Boolean checked) {
        this.id = id;
        this.pId = pId;
        this.name = name;
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "ZTree{" +
                "id='" + id + '\'' +
                ", pId='" + pId + '\'' +
                ", name='" + name + '\'' +
                ", checked=" + checked +
                '}';
    }
}
