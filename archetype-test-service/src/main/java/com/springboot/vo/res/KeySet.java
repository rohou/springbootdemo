package com.springboot.vo.res;

import java.io.Serializable;

/**
 * On 2017/2/16 0016.
 */
public class KeySet <D,T> implements Serializable {
    private D name;
    private T value;

    public D getName() {
        return name;
    }

    public void setName(D name) {
        this.name = name;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "KeySet{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
