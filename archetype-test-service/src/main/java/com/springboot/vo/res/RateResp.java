package com.springboot.vo.res;

/**
 * description:
 * time: 2017/4/27.
 */
public class RateResp {

    private Integer realNumber;

    private Integer totalNumber;

    public Integer getRealNumber() {
        return realNumber;
    }

    public void setRealNumber(Integer realNumber) {
        this.realNumber = realNumber;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
    }
}
