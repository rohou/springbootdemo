package com.springboot.vo.res;

/**
 * On 2017/3/13 0013.
 */
public class CusUserResp {

    /**
     * 账户
     */
    private String username;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 邮箱
     */
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CusUserResp{" +
                "username='" + username + '\'' +
                ", realname='" + realname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
