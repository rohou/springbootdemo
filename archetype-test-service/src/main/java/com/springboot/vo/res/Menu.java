package com.springboot.vo.res;

import java.util.List;

/**
 * On 2017/2/24 0024.
 */
public class Menu {
    private String httpAddress;
    private String menuName;
    private List<Menu> child;

    public Menu() {
    }

    public Menu(String httpAddress, String menuName, List<Menu> child) {
        this.httpAddress = httpAddress;
        this.menuName = menuName;
        this.child = child;
    }

    public String getHttpAddress() {
        return httpAddress;
    }

    public void setHttpAddress(String httpAddress) {
        this.httpAddress = httpAddress;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public List<Menu> getChild() {
        return child;
    }

    public void setChild(List<Menu> child) {
        this.child = child;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "httpAddress='" + httpAddress + '\'' +
                ", menuName='" + menuName + '\'' +
                ", child=" + child +
                '}';
    }
}
