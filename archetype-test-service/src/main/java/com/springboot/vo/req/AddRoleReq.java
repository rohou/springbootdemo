package com.springboot.vo.req;

import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

/**
 * On 2017/3/6 0006.
 */
public class AddRoleReq {


    /**
     * 角色代码
     */
    @NotBlank
    private String roleCode;
    /**
     * 角色名称
     */
    @NotBlank
    private String roleRealName;
    /**
     * 需要新增的权限
     */
    private String orgCode;
    private List<String> needAddAuthorize;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleRealName() {
        return roleRealName;
    }

    public void setRoleRealName(String roleRealName) {
        this.roleRealName = roleRealName;
    }

    public List<String> getNeedAddAuthorize() {
        return needAddAuthorize;
    }

    public void setNeedAddAuthorize(List<String> needAddAuthorize) {
        this.needAddAuthorize = needAddAuthorize;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    @Override
    public String toString() {
        return "AddRoleReq{" +
                "roleCode='" + roleCode + '\'' +
                ", roleRealName='" + roleRealName + '\'' +
                ", orgCode='" + orgCode + '\'' +
                ", needAddAuthorize=" + needAddAuthorize +
                '}';
    }
}
