package com.springboot.vo.req;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

/**
 * On 2017/3/14 0014.
 */
public class UpdateUserReq {
    /**
     * 需要更新的用户帐号
     */
    @NotBlank
    private String username;
    /**
     * 需要更新的用户名字
     */
    @NotBlank
    private String realName;
    /**
     * 需要更新的用户邮箱
     */
    @NotBlank
    @Email
    private String email;
    /**
     * 需要更新的角色
     */
    private List<UpdateItem> updateRoles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<UpdateItem> getUpdateRoles() {
        return updateRoles;
    }

    public void setUpdateRoles(List<UpdateItem> updateRoles) {
        this.updateRoles = updateRoles;
    }

    @Override
    public String toString() {
        return "UpdateUserReq{" +
                "username='" + username + '\'' +
                ", realName='" + realName + '\'' +
                ", email='" + email + '\'' +
                ", updateRoles=" + updateRoles +
                '}';
    }
}
