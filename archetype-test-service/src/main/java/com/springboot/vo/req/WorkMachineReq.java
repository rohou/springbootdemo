package com.springboot.vo.req;

/**
 * On 2017/3/23 0023.
 */
public class WorkMachineReq extends TimeRangeTableReq{
    /**
     * 帐号
     */
    private String username;
    /**
     * 功能号
     */
    private String action;
    /**
     * 客户端IP
     */
    private String clientIp;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * mac地址
     */
    private String macAddress;
    /**
     * 营业部号
     */
    private String brandNo;
    /**
     * 用户分类
     */
    private String userType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getBrandNo() {
        return brandNo;
    }

    public void setBrandNo(String brandNo) {
        this.brandNo = brandNo;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "WorkMachineReq{" +
                "username='" + username + '\'' +
                ", action='" + action + '\'' +
                ", clientIp='" + clientIp + '\'' +
                ", mobile='" + mobile + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", brandNo='" + brandNo + '\'' +
                ", userType='" + userType + '\'' +
                '}';
    }
}
