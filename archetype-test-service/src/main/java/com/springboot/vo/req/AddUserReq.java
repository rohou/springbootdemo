package com.springboot.vo.req;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

/**
 * On 2017/3/13 0013.
 */
public class AddUserReq {
    /**
     * 帐号
     */
    @NotBlank
    private String userName;
    /**
     * 真实姓名
     */
    @NotBlank
    private String realName;
    /**
     * 邮箱
     */
    @Email
    @NotBlank
    private String email;

    /**
     * 需要的角色
     */
    private List<String> needRoles;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getNeedRoles() {
        return needRoles;
    }

    public void setNeedRoles(List<String> needRoles) {
        this.needRoles = needRoles;
    }

    @Override
    public String toString() {
        return "AddUserReq{" +
                "userName='" + userName + '\'' +
                ", realName='" + realName + '\'' +
                ", email='" + email + '\'' +
                ", needRoles=" + needRoles +
                '}';
    }
}
