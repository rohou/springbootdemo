package com.springboot.vo.req;

/**
 * On 2017/3/9 0009.
 */
public class UpdateItem {
    private String id;
    private Boolean checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "UpdateItem{" +
                "id='" + id + '\'' +
                ", checked=" + checked +
                '}';
    }
}
