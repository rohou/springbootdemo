package com.springboot.vo.req;

import java.io.Serializable;

/**
 * On 2017/3/23 0023.
 */
public class TimeRangeTableReq extends TimeRangeReq implements Serializable{
    private Integer limit;
    private Integer offset;
    private String sort;
    private String order;

    public TimeRangeTableReq() {
    }

    public TimeRangeTableReq(Integer limit, Integer offset) {
        this.limit = limit;
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
    public Integer getPage (){
        return (offset+1)/limit;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }


    @Override
    public String toString() {
        return "BaseTableReq{" +
                "limit=" + limit +
                ", offset=" + offset +
                ", sort='" + sort + '\'' +
                ", order='" + order + '\'' +
                '}';
    }
}
