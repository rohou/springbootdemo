package com.springboot.vo.req;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * On 2017/3/9 0009.
 */
public class UpdateRoleReq {

    /**
     * 角色ID
     */
    @NotNull
    private String roleId;
    /**
     * 角色代码
     */
    @NotBlank
    private String roleCode;
    /**
     * 角色名称
     */
    @NotBlank
    private String roleRealName;

    private String orgCode;
    /**
     * 需要更新的权限
     */
    private List<UpdateItem> updateAddAuthorize;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleRealName() {
        return roleRealName;
    }

    public void setRoleRealName(String roleRealName) {
        this.roleRealName = roleRealName;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public List<UpdateItem> getUpdateAddAuthorize() {
        return updateAddAuthorize;
    }

    public void setUpdateAddAuthorize(List<UpdateItem> updateAddAuthorize) {
        this.updateAddAuthorize = updateAddAuthorize;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "UpdateRoleReq{" +
                "roleId='" + roleId + '\'' +
                ", roleCode='" + roleCode + '\'' +
                ", roleRealName='" + roleRealName + '\'' +
                ", orgCode='" + orgCode + '\'' +
                ", updateAddAuthorize=" + updateAddAuthorize +
                '}';
    }
}
