package com.springboot.vo.req;

/**
 * On 2017/3/27 0027.
 */
public class DegreeTimeRangeReq extends TimeRangeReq{
    /**
     * 时间纬度  hourly,daily,weekly,monthly
     */
    private String degree;

    public String getDegree() {
        return degree;
    }


    public String getFormatter (){
        switch (degree){
            case "hourly":return "yyyyMMddHH";
            case "daily":return "yyyyMMdd";
            case "weekly":return "yyyyWW";
            default:return "yyyyMM";
        }
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    @Override
    public String toString() {
        return "DegreeTimeRangeReq{" +
                "degree='" + degree + '\'' +
                '}';
    }
}
