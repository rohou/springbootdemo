package com.springboot.vo.req;


import com.github.pagehelper.Page;

import java.io.Serializable;

/**
 * On 2017/2/21 0021.
 * bootstrap-table 请求参数
 */
public class BaseTableReq implements Serializable{
    private Integer limit;
    private Integer offset;
    private String sort;
    private String order;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
    public Integer getPage (){
        return (offset+1)/limit;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }


    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "BaseTableReq{" +
                "limit=" + limit +
                ", offset=" + offset +
                ", sort='" + sort + '\'' +
                ", order='" + order + '\'' +
                '}';
    }
}
