package com.springboot.vo.req;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * On 2017/2/16 0016.
 * 时间范围请求参数
 */
public class TimeRangeReq implements Serializable{
    private Date start;
    private Date end;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "TimeRangeReq{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
