package com.springboot.vo.req;

/**
 * On 2017/3/28 0028.
 */
public class WorkMachineTopSlowReq extends DegreeTimeRangeReq{
    /**
     * 列表要TOP几
     */
    private Integer listTopN;
    /**
     * 慢请求要前top几
     */
    private Integer columnTopN;

    public Integer getListTopN() {
        return listTopN;
    }

    public void setListTopN(Integer listTopN) {
        this.listTopN = listTopN;
    }

    public Integer getColumnTopN() {
        return columnTopN;
    }

    public void setColumnTopN(Integer columnTopN) {
        this.columnTopN = columnTopN;
    }

    @Override
    public String toString() {
        return "WorkMachineTop5SlowReq{" +
                "listTopN=" + listTopN +
                ", columnTopN=" + columnTopN +
                '}';
    }
}
