package com.springboot.vo.req;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * com.springboot.vo.req
 * Created by ASUS on 2017/5/2.
 * 12:40
 */
public class UpdateUserPasReq {
    /**
     * 帐号，通过session获取
     */
    @NotNull
    private String username;
    /**
     * 旧密码
     */
    @NotBlank(message = "旧密码不能为空")
    private String oldPass;
    /**
     * 新密码
     */
    @Min(value = 6,message = "新密码长度要大于6")
    private String newPass;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    @Override
    public String toString() {
        return "UpdateUserPasReq{" +
                "username='" + username + '\'' +
                ", oldPass='" + oldPass + '\'' +
                ", newPass='" + newPass + '\'' +
                '}';
    }
}
