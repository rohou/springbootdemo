package com.springboot.utils;

import java.util.regex.Pattern;

public class Assert {
    public static void isBlank(String s, String message){
        /**
         * 如果不为空，抛出异常
         */
        if(s != null && !s.equals("")){
            throw new IllegalArgumentException(message);
        }
    }

    public static void notNull(Object obj, String message){
        if(obj == null){
            throw new IllegalArgumentException(message);
        }
    }

    public static void isNull(Object obj, String message){
        if(obj != null){
            throw new IllegalArgumentException(message);
        }
    }

    public static void isNotBlank(String s, String message){
        /**
         * 如果为空，抛出异常
         */
        if(s == null || s.equals("")){
            throw new IllegalArgumentException(message);
        }
    }

    public static void match(String regex, String source, String message){
        Pattern pattern = Pattern.compile(regex);
        if(!pattern.matcher(source).matches()){
            throw new IllegalArgumentException(message);
        }
    }
}
