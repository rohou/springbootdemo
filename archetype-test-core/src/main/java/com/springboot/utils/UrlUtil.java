package com.springboot.utils;

/**
 * On 2017/2/24 0024.
 * URL 处理工具集
 */
public class UrlUtil {
    private static final String RELATIVE_PATH = ".." + "/";

    /**
     * @param url
     * @return 获取相对路径
     */
    public static String getRelativePath(String url){
        String [] paths = url.split("/");
        if(paths.length < 3){//小于3的认为是根路径
            return ".";
        }else {//多级路径
            StringBuilder relativePath = new StringBuilder();
            for (int i = 1; i < paths.length-1;i++){
                relativePath.append(RELATIVE_PATH);
            }
            relativePath.deleteCharAt(relativePath.length()-1);
            return relativePath.toString();
        }
    }
}
