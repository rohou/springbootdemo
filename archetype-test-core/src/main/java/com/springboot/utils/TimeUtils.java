package com.springboot.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * On 2016/11/29 0029.
 *
 * @description 时间公用工具
 */
public class TimeUtils {
    private static ZoneId defaultZone=ZoneId.systemDefault();
    /**
     * @return 获取当天的开始时间
     */
    public static Date getNowDayFirstTime(){
        return getDayFirstTime(new Date());
    }

    public static Date getDayFirstTime(Date date){
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        return cal.getTime();
    }

    public static Date getDayLastTime(Date date){
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,59);
        cal.set(Calendar.MILLISECOND,999);
        return cal.getTime();
    }

    /**
     * 日期加N天
     * @param date
     * @param unit
     * @return
     */
    public static Date addDay(Date date, int unit){
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE,unit);
        return cal.getTime();
    }

    /**
     * 日期加N个月
     * @param date
     * @param unit
     * @return
     */
    public static Date addMonth(Date date,int unit){
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.MONTH,unit);
        return cal.getTime();
    }
    /**
     * @param d 获得当前周的第一天 为周日
     * @return
     */
    public static Date getFirstWeekDay (Date d){
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.SUNDAY);
        c.setTime(d);
        c.set(Calendar.DAY_OF_WEEK,c.getFirstDayOfWeek());
        return c.getTime();
    }
    /**
     * @param d 获得当前周的第一天 为周一
     * @return
     */
    public static Date getFirstWeekDayInMonday(Date d){
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(d);
        c.set(Calendar.DAY_OF_WEEK,c.getFirstDayOfWeek());
        return c.getTime();
    }

    /**
     * @param d 获得当前周的最后一天 为周六
     * @return
     */
    public static Date getLastWeekDay(Date d){
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.SUNDAY);
        c.setTime(d);
        c.set(Calendar.DAY_OF_WEEK,c.getFirstDayOfWeek()+6);
        return c.getTime();
    }
    /**
     * @param d 获得当前周的最后一天 为周日
     * @return
     */
    public static Date getLastWeekDayInMonday(Date d){
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(d);
        c.set(Calendar.DAY_OF_WEEK,c.getFirstDayOfWeek()+6);
        return c.getTime();
    }
    /**
     * @param d 获取当前月第一天
     * @return
     */
    public static Date getFirstMonthDay(Date d){
        Calendar c= new GregorianCalendar();
        c.setTime(d);
        c.set(Calendar.DAY_OF_MONTH,1);
        return c.getTime();
    }
    public static Date getLastMonthDay(Date d){
        Calendar c= new GregorianCalendar();
        c.setTime(d);
        c.set(Calendar.DAY_OF_MONTH,c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return c.getTime();
    }
    /**
     * @param time
     * @return 把"yyyy-MM-dd"字符串 转为date对象
     */
    public static Date convertDate(String time){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dt = null;
        try {
            dt = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dt;
    }


    public static void main (String []args){
        System.out.println(getNowDayFirstTime().getTime());
        System.out.println(getDayLastTime(getNowDayFirstTime()).getTime());
    }
}
