package com.springboot.utils;

import java.util.UUID;

/**
 * On 2017/3/10 0010.
 */
public class UuidUtils {
    public static String build(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-","");
    }
    public static void main(String[] args) {
        UUID uuid = UUID.randomUUID();
        System.out.println(uuid.toString().replace("-",""));
        System.out.println(uuid.toString().replace("-","").length());
    }
}
