package com.springboot.constants;

/**
 * com.springboot.constants
 * Created by ASUS on 2017/4/28.
 * 8:39
 */
public interface DegreeConstants {
    /**
     * 每日统计
     */
    String DAY="daily";
    /**
     * 每月统计
     */
    String MONTH="monthly";
}
