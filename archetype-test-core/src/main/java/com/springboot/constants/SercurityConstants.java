package com.springboot.constants;

/**
 * On 2017/2/27 0027.
 * Spring Security 常量
 */
public interface SercurityConstants {
    /**
     * 默认验证前要加的前缀
     */
    String prefix= "ROLE_";
}
