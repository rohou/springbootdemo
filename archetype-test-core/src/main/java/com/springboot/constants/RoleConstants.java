package com.springboot.constants;

/**
 * On 2017/2/28 0028.
 */
public interface RoleConstants {
    /**
     * 系统默认角色标志
     */
    String SYSTEM_FLAG="0";
    /**
     * 非系统默认角色标志
     */
    String UNSYSTEM_FLAG="1";

    /**
     * 用户已存在错误
     */
    String OPT_ROLE_EXIST="用户已经存在";
    /**
     * 用户插入失败
     */
    String OPT_ROLE_INSERT_ERROR="用户插入失败";
    /**
     * 用户授权失败Msg
     */
    String OPT_ROLE_AUTH_ERROR="用户授权失败";

    String OPT_ROLE_NOEXIST="用户不存在";

    String OPT_ROLE_UPDATE_ERROR="角色更新失败";

    /**
     * 自定义角色根ID
     */
    String ROOT_CUSTOM_ID="1";
    /**
     * 自定义角色根名称
     */
    String ROOT_CUSTOM_NAME="自定义角色";
    /**
     * 系统默认角色根ID
     */
    String ROOT_SYSTEM_ID="0";
    /**
     * 系统默认角色根名称
     */
    String ROOT_SYSTEM_NAME="系统默认角色";
}
