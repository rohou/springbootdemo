package com.springboot.constants;

/**
 * On 2017/3/13 0013.
 */
public interface UserConstants {
    /**
     * 初始密码
     */
    String INIT_PASSWORD="1000:68cabb6d996bcd34485900a723909989:e819d0a84ff7ab594331dc6d9528e655";
    /**
     * 初始性别
     */
    String INIT_SEX="男";
    /**
     * 帐号初始状态
     */
    String INIT_STATUS="1";

    /**
     * 自定义用户
     */
    String CUSTOM_USER="1";

    String OPT_USER_EXIST_ERROR="用户已存在";
    String OPT_USER_NOEXIST_ERROR="用户不存在";
    String OPT_USER_UPDATE_ERROR="用户基本信息更新失败";
}
