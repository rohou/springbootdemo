package com.springboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.ValidatorFactory;

/**
 * On 2017/2/8 0008.
 */
@Configuration
@AutoConfigureAfter(value = {ValidationAutoConfig.class})
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    @Autowired
    private ValidatorFactory validatorFactory;

    @Override
    public Validator getValidator() {
        return (LocalValidatorFactoryBean)validatorFactory;
    }
}
