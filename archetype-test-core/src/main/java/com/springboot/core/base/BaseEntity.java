package com.springboot.core.base;

import java.io.Serializable;

/**
 * On 2017/3/6 0006.
 * 基础的Entity对象,实现序列化接口,方便存储到目标地,如 redis,mongodb 等
 */
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = -4385639479414113220L;
}
