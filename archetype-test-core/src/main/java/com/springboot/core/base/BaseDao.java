package com.springboot.core.base;

/**
 * On 2017/3/8 0008.
 */
public interface BaseDao<T> {
    int insert(T t);

    int update (T t);
}
