package com.springboot.core;

/**
 * On 2017/3/7 0007.
 */
public class OperatorResult {
    /**
     * 操作结果提示
     */
    private String tips;
    /**
     * 结果代码
     */
    private String code;
    /**
     * 自定义信息
     */
    private String msg;

    public OperatorResult(String tips, String code, String msg) {
        this.tips = tips;
        this.code = code;
        this.msg = msg;
    }

    public static OperatorResult SUCCESS(String msg){
        return new OperatorResult("操作成功","200",msg);
    }
    public static OperatorResult SUCCESS(){
        return SUCCESS("");
    }

    public static OperatorResult ERROR(String msg){
        return new OperatorResult("操作失败","500",msg);
    }
    public static  OperatorResult ERROR(){
        return ERROR("");
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "OperatorResult{" +
                "tips='" + tips + '\'' +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
