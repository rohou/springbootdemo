package com.springboot.core.exception;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;

/**
 * On 2017/3/6 0006.
 */
public class BaseRuntimeException extends RuntimeException {
    /**
     * 错误信息类
     */
    private Error error;

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public BaseRuntimeException(Error error) {
        this.error = error;
    }

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public BaseRuntimeException() {
    }

    /**
     * Returns the detail message string of this throwable.
     *
     * @return the detail message string of this {@code Throwable} instance
     * (which may be {@code null}).
     */
    @Override
    public String getMessage() {
        if(ObjectUtils.nullSafeEquals(null,error)){
            return super.getMessage();
        }else{
            return StringUtils.isNotBlank(error.getMessage()) ? error.getMessage():super.getMessage();
        }
    }

    public Error getError() {
        return error;
    }

    /**
     * 设置异常类
     * @param error
     * @return
     */
    public BaseRuntimeException setError(Error error) {
        this.error = error;
        return this;
    }
}
