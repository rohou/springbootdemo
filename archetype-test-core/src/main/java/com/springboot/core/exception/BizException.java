package com.springboot.core.exception;

/**
 * On 2017/3/6 0006.
 */
public class BizException extends BaseRuntimeException{

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     *
     * @param error
     */
    public BizException(Error error) {
        super(error);
    }

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     */
    public BizException() {
    }
}
