package com.springboot.core.exception;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * On 2017/3/6 0006.
 */
@Component
public class ErrorBuilder {
    private static ExceptionProperties properties;
    @Autowired
    public  void setProperties(ExceptionProperties properties) {
        ErrorBuilder.properties = properties;
    }
    /**
     * 构建错误信息
     * @param message       错误信息
     * @return          {@link Error}
     */
    public static Error buildBindError(String message){
        return buildBindError(HttpStatus.BAD_REQUEST.value(),properties.getDefaultErrorCode(),message,"");
    }

    /**
     * 构建错误信息
     * @param message           错误信息
     * @param developerMessage  堆栈信息
     * @return      {@link Error}
     */
    public static Error buildBindError (String message, String developerMessage){
        return buildBindError(HttpStatus.BAD_REQUEST.value(),properties.getDefaultErrorCode(),message,developerMessage);
    }

    /**
     * 基础信息构造器1
     * @param status        状态码
     * @param code          错误码
     * @param message       错误信息
     * @return {@link Error}
     */
    public static Error buildBindError(Integer status, String code, String message) {
        return buildBindError(status,code,message,"");
    }
    /**
     * 基础构造器2
     * @param status    状态码
     * @param code      错误码
     * @param message   错误信息
     * @param developerMessage      开发者错误信息
     * @return {@link Error}
     */
    public static Error buildBindError(Integer status, String code, String message, String developerMessage) {
        if(status == null){
            status = HttpStatus.BAD_REQUEST.value();
        }
        return build(status,properties.getBindExceptionPrefix() + code,message,developerMessage,"","");
    }
    /**
     * 完整构造器
     * @param status            状态码
     * @param code              错误码
     * @param message           错误信息
     * @param developerMessage  开发者错误信息
     * @param moreInfo          url地址
     * @param requestId         请求id号
     * @return {@link Error}
     */
    public static Error build(Integer status, String code, String message, String developerMessage, String moreInfo, String requestId) {
        if(StringUtils.isBlank(code)){
            code = properties.getDefaultErrorCode();
        }
        if(status == null){
            status = properties.getDefaultStatus();
        }
        return new Error(status,code,message,developerMessage,moreInfo,requestId);

    }
}
