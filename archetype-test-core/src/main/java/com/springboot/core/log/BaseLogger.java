package com.springboot.core.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * On 2017/3/6 0006.
 */
public class BaseLogger {
    /**
     * 日志对象
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
}
