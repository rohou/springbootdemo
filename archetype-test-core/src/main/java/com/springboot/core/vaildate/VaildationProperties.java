package com.springboot.core.vaildate;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * On 2017/3/6 0006.
 */
@Component
@ConfigurationProperties(prefix = "system.validation")
public class VaildationProperties {
    /**
     * 是否开启服务层参数验证 默认false
     */
    private Boolean openServiceValidation = Boolean.FALSE;

    public Boolean getOpenServiceValidation() {
        return openServiceValidation;
    }

    public void setOpenServiceValidation(Boolean openServiceValidation) {
        this.openServiceValidation = openServiceValidation;
    }
}
