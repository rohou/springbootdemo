/*
Navicat MySQL Data Transfer

Source Server         : alidocker3307
Source Server Version : 50715
Source Host           : 139.129.23.72:3307
Source Database       : mg_sys

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2017-06-25 11:54:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tsys_dept
-- ----------------------------
DROP TABLE IF EXISTS `tsys_dept`;
CREATE TABLE `tsys_dept` (
  `dept_code` varchar(30) NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `organ_code` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL DEFAULT '',
  `dept_pcode` varchar(30) NOT NULL,
  PRIMARY KEY (`dept_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_dept
-- ----------------------------
INSERT INTO `tsys_dept` VALUES ('000000001', '终端发展部', '000000001', '', '');

-- ----------------------------
-- Table structure for tsys_dict_entry
-- ----------------------------
DROP TABLE IF EXISTS `tsys_dict_entry`;
CREATE TABLE `tsys_dict_entry` (
  `dict_entry_code` varchar(30) NOT NULL,
  `dict_entry_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  PRIMARY KEY (`dict_entry_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_dict_entry
-- ----------------------------
INSERT INTO `tsys_dict_entry` VALUES ('database_label', '数据库标识', '');
INSERT INTO `tsys_dict_entry` VALUES ('transaction_machine', '事务处理机字段字典', '');

-- ----------------------------
-- Table structure for tsys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `tsys_dict_item`;
CREATE TABLE `tsys_dict_item` (
  `dict_entry_code` varchar(30) NOT NULL,
  `dict_item_code` varchar(128) NOT NULL,
  `dict_item_name` varchar(50) NOT NULL,
  `dict_item_order` int(11) NOT NULL,
  UNIQUE KEY `id` (`dict_entry_code`,`dict_item_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_dict_item
-- ----------------------------
INSERT INTO `tsys_dict_item` VALUES ('database_label', 'itn', 'ITN', '0');
INSERT INTO `tsys_dict_item` VALUES ('database_label', 'uat-ymd', 'UAT-YMD', '1');
INSERT INTO `tsys_dict_item` VALUES ('transaction_machine', '', '', '0');

-- ----------------------------
-- Table structure for tsys_menu
-- ----------------------------
DROP TABLE IF EXISTS `tsys_menu`;
CREATE TABLE `tsys_menu` (
  `menu_name` varchar(20) NOT NULL COMMENT '菜单名',
  `menu_id` varchar(30) NOT NULL COMMENT '菜单ID',
  `menu_type` char(1) NOT NULL COMMENT '菜单类型，0为菜单，1为链接',
  `menu_pid` varchar(30) NOT NULL COMMENT '菜单父类',
  `http_address` varchar(100) NOT NULL COMMENT '访问地址',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_menu
-- ----------------------------
INSERT INTO `tsys_menu` VALUES ('资源管理', '09', '0', '', '');
INSERT INTO `tsys_menu` VALUES ('用户', '0901', '0', '09', '/superadmin/manage_user.html');
INSERT INTO `tsys_menu` VALUES ('查看自定义用户', '090101', '1', '0901', '/user/custom/all');
INSERT INTO `tsys_menu` VALUES ('查看用户角色', '090102', '1', '0901', '/user/role/get');
INSERT INTO `tsys_menu` VALUES ('添加用户', '090103', '1', '0901', '/user/add');
INSERT INTO `tsys_menu` VALUES ('用户更新', '090104', '1', '0901', '/user/update');
INSERT INTO `tsys_menu` VALUES ('用户删除', '090105', '1', '0901', '/user/delete');
INSERT INTO `tsys_menu` VALUES ('用户密码重置', '090106', '1', '0901', '/user/password/reset');
INSERT INTO `tsys_menu` VALUES ('角色', '0902', '0', '09', '/manageRole');
INSERT INTO `tsys_menu` VALUES ('查看自定义角色', '090201', '1', '0902', '/role/all/custom');
INSERT INTO `tsys_menu` VALUES ('查看系统角色', '090202', '1', '0902', '/role/all/system');
INSERT INTO `tsys_menu` VALUES ('查看权限列表', '090203', '1', '0902', '/role/all/permission');
INSERT INTO `tsys_menu` VALUES ('角色创建', '090204', '1', '0902', '/role/create');
INSERT INTO `tsys_menu` VALUES ('角色更新', '090205', '1', '0902', '/role/update');
INSERT INTO `tsys_menu` VALUES ('角色删除', '090206', '1', '0902', '/role/delete');

-- ----------------------------
-- Table structure for tsys_organ
-- ----------------------------
DROP TABLE IF EXISTS `tsys_organ`;
CREATE TABLE `tsys_organ` (
  `organ_code` varchar(30) NOT NULL,
  `organ_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `organ_flag` char(1) NOT NULL,
  `organ_pcode` varchar(30) NOT NULL DEFAULT '' COMMENT '父组织结构',
  PRIMARY KEY (`organ_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_organ
-- ----------------------------
INSERT INTO `tsys_organ` VALUES ('000000001', '恒生云赢', '', '1', '');

-- ----------------------------
-- Table structure for tsys_role
-- ----------------------------
DROP TABLE IF EXISTS `tsys_role`;
CREATE TABLE `tsys_role` (
  `role_id` varchar(50) NOT NULL COMMENT '角色ID',
  `role_name` varchar(20) NOT NULL COMMENT '角色名',
  `role_flag` char(1) NOT NULL COMMENT '角色状态',
  `org_code` varchar(30) DEFAULT NULL COMMENT '组织代码',
  `role_realname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_role
-- ----------------------------
INSERT INTO `tsys_role` VALUES ('1000000000', 'superadmin', '0', '000000001', '超级管理员');
INSERT INTO `tsys_role` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', 'admin999', '1', '000111000', '管理员999');
INSERT INTO `tsys_role` VALUES ('7bf5ca55560a4ef5bf5dd5fc28ebc435', '1', '1', '1', 'zhangs');
INSERT INTO `tsys_role` VALUES ('df90121c8c6a490796579600dbfc2092', '00001', '1', '00001', 'test');

-- ----------------------------
-- Table structure for tsys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tsys_role_menu`;
CREATE TABLE `tsys_role_menu` (
  `role_id` varchar(50) NOT NULL,
  `menu_id` varchar(30) DEFAULT NULL,
  UNIQUE KEY `_id` (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_role_menu
-- ----------------------------
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '01');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0101');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0102');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0103');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0104');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '03');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0301');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0302');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0303');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0304');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '09');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0901');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090101');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090102');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090103');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090104');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090105');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090106');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '0902');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090201');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090202');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090203');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090204');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090205');
INSERT INTO `tsys_role_menu` VALUES ('1000000000', '090206');
INSERT INTO `tsys_role_menu` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', '09');
INSERT INTO `tsys_role_menu` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', '0901');
INSERT INTO `tsys_role_menu` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', '090101');
INSERT INTO `tsys_role_menu` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', '090102');
INSERT INTO `tsys_role_menu` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', '090104');
INSERT INTO `tsys_role_menu` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', '090105');
INSERT INTO `tsys_role_menu` VALUES ('584acb22c50a4f36b5a9d8c8b8409bef', '090106');
INSERT INTO `tsys_role_menu` VALUES ('7bf5ca55560a4ef5bf5dd5fc28ebc435', '01');
INSERT INTO `tsys_role_menu` VALUES ('7bf5ca55560a4ef5bf5dd5fc28ebc435', '0101');
INSERT INTO `tsys_role_menu` VALUES ('7bf5ca55560a4ef5bf5dd5fc28ebc435', '0102');
INSERT INTO `tsys_role_menu` VALUES ('7bf5ca55560a4ef5bf5dd5fc28ebc435', '0103');
INSERT INTO `tsys_role_menu` VALUES ('7bf5ca55560a4ef5bf5dd5fc28ebc435', '0104');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '09');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '0901');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090101');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090102');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090103');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090104');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090105');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090106');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '0902');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090201');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090202');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090203');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090204');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090205');
INSERT INTO `tsys_role_menu` VALUES ('df90121c8c6a490796579600dbfc2092', '090206');

-- ----------------------------
-- Table structure for tsys_user
-- ----------------------------
DROP TABLE IF EXISTS `tsys_user`;
CREATE TABLE `tsys_user` (
  `user_name` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `real_name` varchar(50) NOT NULL,
  `sex` char(4) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(32) NOT NULL,
  `user_status` char(1) NOT NULL,
  `user_type` char(1) DEFAULT NULL,
  `dept_code` varchar(30) DEFAULT NULL,
  `organ_code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_user
-- ----------------------------
INSERT INTO `tsys_user` VALUES ('admin', '1000:68cabb6d996bcd34485900a723909989:e819d0a84ff7ab594331dc6d9528e655', '管理员', '男', null, '472327024@qq.com', '1', '1', null, null);
INSERT INTO `tsys_user` VALUES ('jlh', '1000:2b459a223c13853b44047179231f9802:eb4742513e455e08d6df702b9477418a', '季罗浩', '男', '18906672229', '472327024@qq.com', '1', '0', '000000001', '000000001');

-- ----------------------------
-- Table structure for tsys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tsys_user_role`;
CREATE TABLE `tsys_user_role` (
  `user_name` varchar(50) NOT NULL,
  `role_id` varchar(50) NOT NULL,
  KEY `username` (`user_name`),
  KEY `roleid` (`role_id`),
  CONSTRAINT `roleid` FOREIGN KEY (`role_id`) REFERENCES `tsys_role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `username` FOREIGN KEY (`user_name`) REFERENCES `tsys_user` (`user_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsys_user_role
-- ----------------------------
INSERT INTO `tsys_user_role` VALUES ('jlh', '1000000000');
INSERT INTO `tsys_user_role` VALUES ('admin', '1000000000');
INSERT INTO `tsys_user_role` VALUES ('admin', '584acb22c50a4f36b5a9d8c8b8409bef');
INSERT INTO `tsys_user_role` VALUES ('admin', '7bf5ca55560a4ef5bf5dd5fc28ebc435');

-- ----------------------------
-- View structure for bill_goods_view
-- ----------------------------
DROP VIEW IF EXISTS `bill_goods_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `bill_goods_view` AS select `work_goods`.`goods_id` AS `goods_id`,`work_bill`.`bill_id` AS `bill_id`,`work_bill_goods`.`number` AS `number`,`work_goods`.`goods_name` AS `goods_name`,`work_bill_goods`.`goods_price` AS `goods_price` from ((`work_bill` join `work_bill_goods` on((`work_bill`.`bill_id` = `work_bill_goods`.`bill_id`))) join `work_goods` on((`work_bill_goods`.`goods_id` = `work_goods`.`goods_id`))) ;

-- ----------------------------
-- View structure for income_view
-- ----------------------------
DROP VIEW IF EXISTS `income_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `income_view` AS select `work_bill`.`total_income` AS `income`,`work_bill`.`update_time` AS `update_time` from `work_bill` ;

-- ----------------------------
-- View structure for room_bill_view
-- ----------------------------
DROP VIEW IF EXISTS `room_bill_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `room_bill_view` AS select `work_bill`.`bill_id` AS `bill_id`,`work_room`.`price` AS `price`,`work_accommodation_record`.`start_time` AS `start_time`,`work_bill`.`hotel_expense` AS `hotel_expense` from ((`work_accommodation_record` join `work_bill` on((`work_accommodation_record`.`bill_id` = `work_bill`.`bill_id`))) join `work_room` on((`work_room`.`room_number` = `work_accommodation_record`.`room_number`))) where ((`work_room`.`status` = 'IN_USE') and isnull(`work_accommodation_record`.`end_time`)) ;
