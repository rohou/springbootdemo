
##菜单添加
SET @menuName='慢请求查看';
SET @menuId='030404';
SET @menuType='1';
SET @menuPid='0304';
SET @httpAddress='/log/work/queryTopNSlowReq';
SET @roleId='1000000000';
#插入菜单SQL
INSERT INTO tsys_menu (menu_name,menu_id,menu_type,menu_pid,http_address) VALUES(@menuName,@menuId,@menuType,@menuPid,@httpAddress);
INSERT INTO tsys_role_menu (menu_id,role_id) VALUES(@menuId,@roleId);

##删除菜单

SET @menuId='03';
DELETE FROM tsys_role_menu WHERE menu_id like CONCAT(@menuId,'%');
DELETE FROM tsys_menu WHERE menu_id like CONCAT(@menuId,'%');
