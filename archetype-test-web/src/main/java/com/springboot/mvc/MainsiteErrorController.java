package com.springboot.mvc;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * On 2017/2/8 0008.
 * Controller 错误处理
 */
@Controller
public class MainsiteErrorController implements ErrorController {

    private static final String ERROR_PATH = "/error";

    @RequestMapping(value = ERROR_PATH,method = RequestMethod.GET)
    public String e404(){
        return "404";
    }
    @Override
    public String getErrorPath() {
        return "";
    }
}
