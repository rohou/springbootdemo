package com.springboot.mvc;

import com.springboot.utils.UrlUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * On 2017/2/8 0008.
 * 默认.html页面跳转
 */
@Controller
public class PageController {

    @Autowired
    private UrlPathHelper urlPathHelper;

    @RequestMapping("/**/*.html")
    public String index(HttpServletRequest request){
        String url = urlPathHelper.getOriginatingServletPath(request);
        url = StringUtils.replaceOnce(url,"/","");
        url = StringUtils.remove(url,".html");
        return url;
    }

    @RequestMapping("/**.html")
    public String forward2(HttpServletRequest request){
        String url = urlPathHelper.getOriginatingServletPath(request);
        url = StringUtils.replaceOnce(url,"/","");
        url = StringUtils.remove(url,".html");
        return url;
    }



}
