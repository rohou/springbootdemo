package com.springboot.controller;

import com.springboot.core.dictionary.DictEntry;
import com.springboot.core.dictionary.DictionayUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * On 2017/3/22 0022.
 */
@RestController
@RequestMapping("/dict")
public class DictionaryController {
    @RequestMapping(value = "/get",method = RequestMethod.GET)
    public DictEntry get (String entry){
        return DictionayUtils.findEntry(entry);
    }
}
