package com.springboot.controller;

import com.springboot.constants.SercurityConstants;
import com.springboot.sql.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * On 2017/2/23 0023.
 * 面板页面控制器
 */
@Controller
public class DashboardController {
    private final IMenuService iMenuService;
    @Autowired
    public DashboardController(IMenuService iMenuService) {
        this.iMenuService = iMenuService;
    }

    @RequestMapping(value = "/dashboard")
    public String get (HttpServletRequest httpServletRequest,Model model){
        SecurityContextImpl securityContextImpl = (SecurityContextImpl) httpServletRequest
                .getSession().getAttribute("SPRING_SECURITY_CONTEXT");
        List<String> authories=securityContextImpl.getAuthentication().getAuthorities().stream().map(m->
                m.getAuthority()
                        //截取ROLE_前缀
                .substring(m.getAuthority().indexOf(SercurityConstants.prefix)+SercurityConstants.prefix.length()))
                .collect(Collectors.toList());
        //获取用户对应的菜单
        model.addAttribute("menus",iMenuService.getMenuByRoles(authories));
        return "dashboard";
    }
}
