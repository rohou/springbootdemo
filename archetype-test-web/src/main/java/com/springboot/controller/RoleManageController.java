package com.springboot.controller;

import com.springboot.core.OperatorResult;
import com.springboot.sql.dao.entity.SysRole;
import com.springboot.sql.service.IMenuService;
import com.springboot.sql.service.IRoleService;
import com.springboot.vo.req.AddRoleReq;
import com.springboot.vo.req.UpdateRoleReq;
import com.springboot.vo.res.ZTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * On 2017/2/28 0028.
 * 角色管理的控制器
 */
@RestController
@RequestMapping("/role")
public class RoleManageController {
    private final IRoleService iRoleService;
    private final IMenuService iMenuService;
    @Autowired
    public RoleManageController(IRoleService iRoleService, IMenuService iMenuService) {
        this.iRoleService = iRoleService;
        this.iMenuService = iMenuService;
    }

    /**
     * @return 所有自定义角色
     */
    @RequestMapping(method = RequestMethod.GET,value = "/all/custom")
    public List<SysRole> allCustom(){
        return iRoleService.getAllCustomRole();
    }

    /**
     * @return 所有系统角色
     */
    @RequestMapping(method = RequestMethod.GET,value = "/all/system")
    public List<SysRole> allSystem(){
        return iRoleService.getAllSystemRole();
    }

    /**
     * @return 所有权限集合
     */
    @RequestMapping(method = RequestMethod.GET,value = "/all/permission")
    public List<ZTree> allPermission(String roleId){
        return iMenuService.getAllPermission4ZTree(roleId);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/create")
    public OperatorResult create (@RequestBody AddRoleReq addRoleReq){
        return iRoleService.addNewRole(addRoleReq);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/update")
    public OperatorResult update (@RequestBody UpdateRoleReq updateRoleReq){
        return iRoleService.updateRole(updateRoleReq);
    }

    @RequestMapping(method = RequestMethod.POST ,value = "/delete")
    public OperatorResult delete (@RequestBody List<String> roleIds){
        return iRoleService.deleteRole(roleIds);
    }
}
