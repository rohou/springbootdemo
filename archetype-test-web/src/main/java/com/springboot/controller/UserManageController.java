package com.springboot.controller;

import com.springboot.core.OperatorResult;
import com.springboot.sql.service.IUserService;
import com.springboot.vo.req.AddUserReq;
import com.springboot.vo.req.UpdateUserPasReq;
import com.springboot.vo.req.UpdateUserReq;
import com.springboot.vo.res.CusUserResp;
import com.springboot.vo.res.ZTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * On 2017/3/13 0013.
 */
@RestController
@RequestMapping("/user")
public class UserManageController {
    private final IUserService iUserService;
    @Autowired
    public UserManageController(IUserService iUserService) {
        this.iUserService = iUserService;
    }

    /**
     * @return 自定义用户获取
     */
    @RequestMapping(method = RequestMethod.GET,value = "/custom/all")
    public List<CusUserResp> customAll(){
        return iUserService.getAllCusUsers();
    }

    /**
     * @param username 帐号
     * @return 指定用户的权限列表
     */
    @RequestMapping(method = RequestMethod.GET,value = "/role/get")
    public List<ZTree> roleGet (String username){
        return iUserService.getUserRoles(username);
    }

    /**
     * @param addUserReq 添加用户的参数
     * @return  操作结果
     */
    @RequestMapping(method = RequestMethod.POST,value = "/add")
    public OperatorResult add (@RequestBody AddUserReq addUserReq){
        return iUserService.insertUser(addUserReq);
    }

    /**
     * @param updateUserReq 需要更新用户的参数
     * @return  操作结果
     */
    @RequestMapping(method = RequestMethod.POST,value = "/update")
    public OperatorResult update (@RequestBody UpdateUserReq updateUserReq){
        return iUserService.updateUser(updateUserReq);
    }

    /**
     * @param usernames 需要删除的账户列表
     * @return  操作结果
     */
    @RequestMapping(method = RequestMethod.POST,value = "/delete")
    public OperatorResult delete (@RequestBody List<String> usernames){
        return iUserService.deleteUser(usernames);
    }

    /**
     * @param username 用户帐号
     * @return 密码重置操作结果
     */
    @RequestMapping(method = RequestMethod.GET,value = "/password/reset")
    public OperatorResult passwordReset (String username){
        return iUserService.initPassWord(username);
    }
    @RequestMapping("/password/update")
    public OperatorResult passwordUpdate (UpdateUserPasReq updateUserPasReq, HttpServletRequest httpServletRequest){
        SecurityContextImpl securityContextImpl = (SecurityContextImpl) httpServletRequest
                .getSession().getAttribute("SPRING_SECURITY_CONTEXT");
        String as=(String)securityContextImpl.getAuthentication().getPrincipal();
        updateUserPasReq.setUsername(as);
        return iUserService.updatePassWord(updateUserPasReq);
    }
}
