import com.springboot.sql.dao.TSysMenuDao;
import com.springboot.sql.dao.entity.SysMenu;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by jlh
 * On 2016/12/27 0027.
 *
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class test {

    @Autowired
    private TSysMenuDao tSysMenuDao;
    @Test
    public void test (){
//        TbUser tbUser = new TbUser();
//        tbUser.setId(1001L);
//        tbUser.setAge(18);
//        tbUser.setName("jlh");
//        //mock数据测试
//        List<TbUser> tbUsers= new ArrayList<TbUser>();
//        tbUsers.add(tbUser);
//        TbUserDao mock= EasyMock.createMock(TbUserDao.class);
//        EasyMock.expect(mock.findAll()).andReturn(tbUsers);
//        EasyMock.replay(mock);
//
//        TbUserServiceImpl tbUserService = new TbUserServiceImpl(mock);
//        System.out.println(tbUserService.findAll());
    }
    @Test
    public void testCollection (){
        SysMenu condition =new SysMenu();
        condition.setMenuType("0");
        List<SysMenu> sysMenuList=tSysMenuDao.findExt(condition);
        System.out.println(sysMenuList);
    }
}
